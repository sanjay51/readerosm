package Data;

import java.io.InputStream;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;

import android.content.Context;
import android.util.Log;
import android.util.Xml;
import model.Story;

public class DataManager {
    ArrayList<Story> StoryList;
    static DataManager manager1 = null;
    static DataManager manager2 = null;
    int Volume = 1;
    Context context;

    public String getTitle(int index) {
        return StoryList.get(index).getTitle();
    }

    public Story getStory(int index) {
        return StoryList.get(index);
    }

    public ArrayList<String> getTitleList() {
        ArrayList<String> titleList = new ArrayList<String>();
        for (Story story : StoryList) {
            titleList.add(story.getTitle());
        }
        return titleList;
    }

    public static ArrayList<String> getVolumeList() {
        ArrayList<String> volumeList = new ArrayList<String>();
        volumeList.add("Contents");
        volumeList.add("About");
        return volumeList;
    }

    public static DataManager getManager(Context context, int volume) {
        if(volume == 1) {
            if(manager1 == null) {
                manager1 = new DataManager(context, volume);
            }

            return manager1;
        } else if (volume ==2) {
            if (manager2 == null) {
                manager2 = new DataManager(context, volume);
            }
        }
        return manager2;
    }

    private DataManager(Context context, int volume) {
        StoryList = new ArrayList<Story>();
        this.context = context;
        this.Volume = volume;

        if (volume == 1)
            doXMLStuff("content.xml");
        else if (volume == 2)
            doXMLStuff("about.xml");

    }

    public void doXMLStuff(String fileName) {
        try {
            XmlPullParser parser = Xml.newPullParser();
            int count = 1;

            InputStream in_s = context.getAssets().open(fileName);

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in_s, null);

            int eventType = parser.getEventType();
            Story story = null;

            while (eventType != XmlPullParser.END_DOCUMENT) {
                String name = null;
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        break;
                    case XmlPullParser.START_TAG:
                        name = parser.getName();
                        if (name.equals("title")) {
                            story = new Story();
                            story.title = (count++) + ". " + parser.nextText();
                        } else if (name.equals("content")) {
                            story.content = parser.nextText();
                            StoryList.add(story);
                        }
                        break;
                }
                eventType = parser.next();
            }

        } catch (Exception e) {
            Log.e("error", "error" + e.getLocalizedMessage());
        }
    }

}
