package com.codingg.readerOSM;

import java.util.ArrayList;

import Data.DataManager;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity extends Activity {

    public Activity This = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        This = this;

        initializeIndex();
    }

    public void initializeIndex() {
        final ListView listview = (ListView) findViewById(R.id.listIndex);
        String[] values = new String[] { "Volume.I (56)", "Volume.II (64)" };

        final ArrayList<String> list =  DataManager.getVolumeList();

        final StableArrayAdapter adapter = new StableArrayAdapter(this,
                android.R.layout.simple_list_item_1, list);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                Intent i = new Intent(This, VolumeActivity.class);
                i.putExtra("volume", position+1+"");
                startActivity(i);
            }

        });
    }
}
