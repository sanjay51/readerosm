package model;

public class Story {
    public String title;
    public String content;

    public Story() {
        this.title = "null";
        this.content = "null";
    }
    public Story(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return this.title;
    }

    public String getContent() {
        return this.content;
    }

}
